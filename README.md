# VirtualEnvironments

Muchas Gracias a todos por asistír.

		 Christian David Moreno Uribe
	    (Sr?) Python Developer y Data Engineer
			Source Meridian

---------------------------------------------------------------
- Virtualenv
- Venv
- Pipenv
----------------------------------------------------------------
- Pyenv
---------------------------------------------------------------

Todo desde un ejemplo y desde lo practico.

---------------------------------------------------------------

## Un ejemplo práctico con Flask

### Qué es Flask?
	R: Flask es un “micro” Framework escrito en Python y 
	concebido para facilitar el desarrollo de Aplicaciones Web 
	bajo el patrón MVC.

### Qué es MVC?
	R: En líneas generales, MVC es una propuesta de arquitectura 
	del software utilizada para separar el código por sus 
	distintas responsabilidades, manteniendo distintas capas 
	que se encargan de hacer una tarea muy concreta, lo que 
	ofrece beneficios diversos.

	- Modelo: Datos
	- Vista: Html
	- Controlador: Python


---------------------------------------------------------------

## Lo primero es asegurarnos de tener pip

pip es un sistema de gestión de paquetes utilizado para instalar y administrar paquetes de software escritos en Python. Muchos paquetes pueden ser encontrados en el Python Package Index (PyPI). Python 2.7.9 y posteriores (en la serie Python2), Python 3.4 y posteriores incluyen pip (pip3 para Python3) por defecto.

[how to install?](https://tecnonucleous.com/2018/01/28/como-instalar-pip-para-python-en-windows-mac-y-linux/)

---------------------------------------------------------------
### modulos necesarios:

- requests
- beautifulsoup4
- flask

### Tradicionalmente sin ambientes virtuales

```sh 
pip install requests
pip install beautifulsoup4
pip install flask
```
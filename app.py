from flask import Flask
from modules import pokemon as poke

app = Flask(__name__, template_folder='test_templates')


page = """
<h1 style="text-align: center;"><em><strong>{pName}!!!</strong></em></h1>
<p style="text-align: center;"><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="{pImage}" alt="" width="400" height="400" /></strong></p>
<table style="height: 54px; width: 73.7991%; border-collapse: collapse; margin-left: auto; margin-right: auto;" border="1">
<tbody>
<tr style="height: 18px; text-align: center;">
<td style="width: 50%; height: 18px; text-align: center;">Nombre</td>
<td style="width: 50%; height: 18px;">{pName}</td>
</tr>
<tr style="height: 18px; text-align: center;">
<td style="width: 50%; height: 18px;">Pokedex</td>
<td style="width: 50%; height: 18px;">{pDex}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 50%; height: 18px; text-align: center;">Tipos</td>
<td style="width: 50%; height: 18px; text-align: center;">{pTypes}</td>
</tr>
</tbody>
</table>

"""
@app.route('/')
def index():
    return '<h1>Hello World!</h1>'

@app.route('/get_pokemon')
def pokemon():
    my_pokemon = poke._get_pokemon()
    pImage = my_pokemon["img"]
    pDex = my_pokemon["kdex"]
    pName = my_pokemon["name"]
    pTypes = my_pokemon["types"]
    print(my_pokemon)
    return page.format(pName=pName, pDex=pDex, pImage=pImage, pTypes=', '.join(pTypes))

if __name__ == '__main__':
    app.run(debug = True, port = 8080)


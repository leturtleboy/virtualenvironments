from bs4 import BeautifulSoup
import requests
import random


URL = "https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_Kanto_Pok%C3%A9dex_number"


def _get_page_text(url: str):
    response = requests.get(url)
    # print(response.status_code)
    if response.status_code == 200:
        content = response.text
        return content
    return None


def _get_pokemons(content: str):
    result = dict()
    soup = BeautifulSoup(content, "html.parser")
    tables = soup.find_all("table")[1:6]
    for table in tables:
        tags = table.find_all("tr", {"style": "background:#FFF"})
        for tag in tags:
            dex = tag.find_all("td")
            kdex = dex[0].text[:-1]
            ndex = dex[1].text[:-1]
            name = tag.find("a")["title"]
            span_tags = tag.find_all("span")
            img = "https:" + tag.find("img")["src"]
            types = [pokemon_type.text for pokemon_type in span_tags]
            structure = {"kdex": kdex, "ndex": ndex, "name": name, "img": img, "types": types}
            if not result.get(name):
                result[name] = [structure]
            else:
                result[name].append(structure)
    return result


def _get_random_pokemon(pokemons):
    key = random.choice(list(pokemons.keys()))
    return pokemons[key][0]


def _get_pokemon():
    pokemons = _get_pokemons(_get_page_text(URL))
    return _get_random_pokemon(pokemons)